#!/usr/bin/python3
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from urllib.error import HTTPError
import json
from datetime import date
import sys
import json
import math

API_URL = 'https://www.strava.com/api/v3/'
CLIENT_ID = ''
CLIENT_SECRET = ''
REFRESH_TOKEN = ''

def refresh_token():
    req = Request(API_URL + 'oauth/token')
    data = urlencode({'client_id': CLIENT_ID, 'client_secret': CLIENT_SECRET, 'grant_type': 'refresh_token', 'refresh_token': REFRESH_TOKEN})
    data = data.encode('ascii')
    try:
        response = urlopen(req, data).read()
    except HTTPError as e:
        print('[!] Error: ' + str(e.code) + ":" + str(e.read()))
        sys.exit(1)
    json_data = json.loads(response.decode('utf-8'))

    return json_data["access_token"]

def pull_details(response):
    # Parse JSON
    json_data = json.loads(response.decode('utf-8'))
    for activity in json_data:
        details = activity["athlete"]["firstname"] + ","
        details += activity["athlete"]["lastname"] + ","
        details += activity["type"] + ","
        details += activity["name"] + ","
        # Convert meters to miles
        miles = round((activity["distance"] * 0.00062137),2)
        details += str(miles) + ","
        # Convert moving time to pace
        pace = 26.8224 / (activity["distance"] / activity["moving_time"])
        details += str(round(pace,2)) + ","
        # Convert meters to feet
        details += str(int(round((activity["total_elevation_gain"] * 3.281),0)))
        details += "\n"
        f.write(details)

api_key = refresh_token()

filename = str(date.today()) + ".csv"
f = open('output/' + filename, 'w')
header = 'firstname,lastinitial,type,name,distance(miles),pace(min/mile),elevation_gain(feet)\n'
f.write(header)

# Get data for each page
for page in range(1, 3):

    req = Request(API_URL + 'clubs/212991/activities?page=' + str(page) + '&per_page=200')
    req.add_header('Authorization', 'Bearer ' + api_key)

    try:
        response = urlopen(req).read()
    except HTTPError as e:
        print('[!] Error: ' + str(e.code) + ": " + str(e.read()))
        sys.exit(1)

    pull_details(response)

f.close()